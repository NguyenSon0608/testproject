<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = (new Post())->all();
        return view('post/index', compact('posts'));
    }

    public function create()
    {
        return view('post\add_post');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $title = $data['title'];
        $slug = $data['slug'];
        $description = $data['description'];
        $content = $data['content'];
        $thumbnail = $data['thumbnail'];
        $user_id = $data['user_id'];

        $posts = new Post();
        $posts->title = $title;
        $posts->slug = $slug;
        $posts->description = $description;
        $posts->content = $content;
        $posts->thumbnail = $thumbnail;
        $posts->user_id = $user_id;
        $posts->save();

        return redirect() ->back() ->with('success','Thêm thành công');
    }

    public function edit($id)
    {
        $posts = Post::where('id',$id)->first();
        return view('post\edit_post', compact('posts'));
    }

    public function update(Request $request ,$id)
    {
        $post = Post::find($id);
        $post->update($request->all());
        return redirect() ->back() ->with('success','Sửa thành công');
    }

    public function destroy($id)
    {
//        Post::find($id)->delete();
        return redirect() ->back() ->with('success','Xóa thành công');
    }

    public function show($id)
    {
//        Post::find($id)->delete();
//        return redirect() ->back() ->with('success','Xóa thành công');
    }
}
