<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $users = (new User())->all();
        return view('index', compact('users'));
    }

    public function create()
    {
        return view('add-user');
    }

    public function store(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;

        $users = new User();
        $users->name = $name;
        $users->email = $email;
        $users->password = $password;
        $users->save();

        return redirect() ->back() ->with('success','Thêm users thành công');
    }

    public function edit($id)
    {
        $users = User::where('id',$id)->first();
        return view('edit', compact('users'));
    }

    public function update($id, Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;

        User::where('id','=',$id)->update([
            'name'=>$name,
            'email'=>$email,
            'password'=>$password,
        ]);
        return redirect() ->back() ->with('success','Update users thành công');
    }

    public function destroy($id)
    {
//        User::find($id)->delete();
//        return redirect() ->back() ->with('success','Xóa users thành công');
    }

    public function show($id)
    {
        User::where('id',$id)->delete();
        return redirect() ->back() ->with('success','Xóa users thành công');
    }

}
