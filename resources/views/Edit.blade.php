<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ADD-User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">

</head>
<body>
    <div class="container" style="margin-top: 20px">
        <div class="row">
            <div class="col-md-12">
                <h2>Sửa Users</h2>
                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {{Session::get('success')}}
                    </div>
                @endif
                <form method="POST" action="{{route('users.update', $users)}}">
                    @csrf
                    @method("PUT")
                    <input type="hidden" name="id" value="{{$users->id}}">
                    <div class="md-3">
                        <label class="form-lebel">Họ Tên</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter name" value="{{$users->name}}">
                    </div>
                    <div class="md-3">
                        <label class="form-lebel">Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Enter email" value="{{$users->email}}">
                    </div>
                    <div class="md-3">
                        <label class="form-lebel">Password</label>
                        <input type="password" class="form-control" name="password" value="{{$users->password}}">
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{route('users.index')}}" class="btn btn-danger">Thoát</a>
                </form>
            </div>
        </div>
    </div>

</body>
</html>
