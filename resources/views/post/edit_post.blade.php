<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit_post</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">

</head>
<body>
<div class="container" style="margin-top: 20px">
    <div class="row">
        <div class="col-md-12">
            <h2>Sửa bài viết</h2>
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('success')}}
                </div>
            @endif
            <form method="POST" action="{{route('posts.update', $posts)}}">
                @csrf
                @method("PUT")
{{--                <input type="hidden" name="id" value="{{$posts->id}}">--}}
                <div class="md-3">
                    <label class="form-lebel">Title</label>
                    <input type="text" class="form-control" name="title" value="{{$posts->title ?? ''}}">
                </div>
                <div class="md-3">
                    <label class="form-lebel">Slug</label>
                    <input type="text" class="form-control" name="slug" value="{{$posts->slug ?? ''}}">
                </div>
                <div class="md-3">
                    <label class="form-lebel">Description</label>
                    <input style="height: 150px" class="form-control" name="description" value="{{$posts->description ?? ''}}">
                </div>
                <div class="md-3">
                    <label class="form-lebel">Content</label>
                    <input type="text" class="form-control" name="content" value="{{$posts->content ?? ''}}">
                </div>
                <div class="md-3">
                    <label class="form-lebel">image</label>
                    <input type="file" class="form-control" accept="image/*" onchange="loadFile(event)" name="thumbnail" value="{{$posts->thumbnail ?? ''}}">
                    <img class="form-control" style="height: 200px; width: 200px; margin-top: 10px" id="output" />
                    <script>
                        var loadFile = function(event) {
                            var output = document.getElementById('output');
                            output.src = URL.createObjectURL(event.target.files[0]);
                            output.onload = function() {
                                URL.revokeObjectURL(output.src) // free memory
                            }
                        };
                    </script>
{{--                    <input type="file" class="form-control" name="thumbnail" value="{{$posts->thumbnail ?? ''}}">--}}
                </div>
                <div class="md-3">
                    <label class="form-lebel">User_id</label>
                    <input type="text" class="form-control" name="user_id" value="{{$posts->user_id ?? ''}}">
                </div>
                <button type="submit" class="btn btn-primary"> Sửa </button>
                <a href="{{route('posts.index')}}" class="btn btn-danger">Thoát</a>
            </form>
        </div>
    </div>
</div>

</body>
</html>
