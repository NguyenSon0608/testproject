<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ADD_post</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">

</head>
<body>
<div class="container" style="margin-top: 20px">
    <div class="row">
        <div class="col-md-12">
            <h2>Thêm bài viết</h2>
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('success')}}
                </div>
            @endif
            <form method="post" action="{{route('posts.store')}}">
                @csrf
                <div class="md-3">
                    <label class="form-lebel">Title</label>
                    <input type="text" class="form-control" name="title">
                </div>
                <div class="md-3">
                    <label class="form-lebel">Slug</label>
                    <input type="text" class="form-control" name="slug">
                </div>
                <div class="md-3">
                    <label class="form-lebel">Description</label>
                    <input style="height: 150px" class="form-control" name="description">
                </div>
                <div class="md-3">
                    <label class="form-lebel">Content</label>
                    <input type="text" class="form-control" name="content">
                </div>
                <div class="md-3">
                    <label class="form-lebel">image</label>
                    <input type="file" class="form-control" name="thumbnail">
                </div>
                <div class="md-3">
                    <label class="form-lebel">User_id</label>
                    <input type="text" class="form-control" name="user_id">
                </div>
                <button type="submit" class="btn btn-primary">Thêm mới</button>
                <a href="{{route('posts.index')}}" class="btn btn-danger">Thoát</a>
            </form>
        </div>
    </div>
</div>

</body>
</html>
