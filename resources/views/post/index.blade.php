<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách bài viết</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">

</head>
<body>
<div class="container" style="margin-top: 20px">
    <div class="row">
        <div class="col-md-12">
            <h2>Danh sách bài viết</h2>
            <div style="margin-left: 86%">
                <a href="{{route('posts.create')}}" class="btn btn-primary">Thêm bài viết</a>
            </div>
            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('success')}}
                </div>
            @endif
            <table class="table">
                <thead><tr>
                    <th>STT</th>
                    <th>Title</th>
                    <th>Slug</th>
                    <th>Description</th>
                    <th>Content</th>
                    <th>Thumbnail</th>
                    <th>Name</th>
                </tr></thead>
                <tbody>
                @foreach($posts as $key => $post)
                    <tr>
                        <td>{{$key}}</td>
                        <th>{{$post->tile}}</th>
                        <th>{{$post->slug}}</th>
                        <th>{{$post->description}}</th>
                        <th>{{$post->content}}</th>
                        <th>{{$post->thumbnail}}</th>
                        <th>{{$post->user->name ?? ''}}</th>
                        <th><a href="{{route('posts.edit',$post)}}" class="btn btn-primary">Sửa</a> |
                            <form class="btn btn-danger" method="post" action="{{ route('posts.destroy', $post)}}">
                                @method('delete')
                                @csrf
                               <button type="submit">Xóa</button>
                            </form>
{{--                            <a href="{{route('posts.destroy',$post)}}" class="btn btn-danger">Xóa</a></th>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>




