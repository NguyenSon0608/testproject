<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách Users</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">

</head>
<body>
    <div class="container" style="margin-top: 20px">
        <div class="row">
            <div class="col-md-12">
                <h2>Danh sách Users</h2>
                <div style="margin-left: 86%">
                    <a href="{{route('users.create')}}" class="btn btn-primary">Thêm mới</a>
                </div>
                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {{Session::get('success')}}
                    </div>
                @endif
                <table class="table">
                    <thead><tr>
                        <th>STT</th>
                        <th>Họ tên</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Action</th>
                    </tr></thead>
                    <tbody>
                    @php
                    $i = 1;
                    @endphp
                        @foreach($users as $user)
                            <tr>
                                <td>{{$i++}}</td>
                                <th>{{$user->name}}</th>
                                <th>{{$user->email}}</th>
                                <th>{{$user->password}}</th>
                                <th><a href="{{route('users.edit',$user)}}" class="btn btn-primary">Sửa</a> |
{{--                                    <form class="btn btn-danger" method="post" action="{{ route('users.destroy', $user) }}">--}}
{{--                                        @method('delete')--}}
{{--                                        @csrf--}}
{{--                                        <div class="form-row">--}}
{{--                                            Xóa--}}
{{--                                        </div>--}}
{{--                                    </form>--}}
                                    <a href="{{route('users.destroy',$user)}}" class="btn btn-danger">Xóa</a></th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>




